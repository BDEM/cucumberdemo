$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("file:src/test/java/com/linos/features/TaskAssign.feature");
formatter.feature({
  "name": "Find list of task",
  "description": "",
  "keyword": "Feature",
  "tags": [
    {
      "name": "@Smoke"
    }
  ]
});
formatter.scenario({
  "name": "Get the task using get method",
  "description": "",
  "keyword": "Scenario",
  "tags": [
    {
      "name": "@Smoke"
    },
    {
      "name": "@Smoke"
    }
  ]
});
formatter.step({
  "name": "as a system engineer i want to assign task",
  "keyword": "Given "
});
formatter.match({
  "location": "com.linos.steps.TaskAssignStepDefs.asASystemEngineerIWantToAssignTask()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "i want to check assign task",
  "keyword": "When "
});
formatter.match({
  "location": "com.linos.steps.TaskAssignStepDefs.iWantToCheckAssignTask()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I find response of the task",
  "keyword": "Then "
});
formatter.match({
  "location": "com.linos.steps.TaskAssignStepDefs.iFindResponseOfTheTask()"
});
formatter.result({
  "status": "passed"
});
formatter.uri("file:src/test/java/com/linos/features/equipment.feature");
formatter.feature({
  "name": "Find list of equipment",
  "description": "",
  "keyword": "Feature"
});
formatter.scenario({
  "name": "Get all equipment",
  "description": "",
  "keyword": "Scenario",
  "tags": [
    {
      "name": "@Regression"
    }
  ]
});
formatter.step({
  "name": "as a system engineer i want to list of equipment",
  "keyword": "Given "
});
formatter.match({
  "location": "com.linos.steps.EquipmentStepDefs.as_a_system_engineer_i_want_to_list_of_equipment()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "i hit the get service",
  "keyword": "When "
});
formatter.match({
  "location": "com.linos.steps.EquipmentStepDefs.i_hit_the_get_service()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I view  all equipment in response body",
  "keyword": "Then "
});
formatter.match({
  "location": "com.linos.steps.EquipmentStepDefs.i_view_all_equipment_in_response_body()"
});
formatter.result({
  "status": "passed"
});
});