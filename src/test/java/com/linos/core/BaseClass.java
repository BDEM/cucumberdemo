package com.linos.core;


import io.restassured.common.mapper.TypeRef;
import org.hamcrest.Matchers;

import java.util.List;
import java.util.Map;

import static io.restassured.RestAssured.get;
import static org.hamcrest.MatcherAssert.assertThat;

import static io.restassured.RestAssured.*;
import static io.restassured.matcher.RestAssuredMatchers.*;
import static org.hamcrest.Matchers.*;
public class BaseClass {


    public static void sampleTest() {


        // Extract
        List<Map<String, Object>> products = get("/products").as(new TypeRef<List<Map<String, Object>>>() {});

        // Now you can do validations on the extracted objects:
        assertThat(products, hasSize(2));
        assertThat(products.get(0).get("id"), Matchers.<Object>equalTo(2));
        assertThat(products.get(0).get("name"), Matchers.<Object>equalTo("An ice sculpture"));
        assertThat(products.get(0).get("price"), Matchers.<Object>equalTo(12.5));
        assertThat(products.get(1).get("id"), Matchers.<Object>equalTo(3));
        assertThat(products.get(1).get("name"), Matchers.<Object>equalTo("A blue mouse"));
        assertThat(products.get(1).get("price"), Matchers.<Object>equalTo(25.5));



    }
    public static void sampleTestTwo() {


        // Extract
        List<Map<String, Object>> products = get("/products").as(new TypeRef<List<Map<String, Object>>>() {});

        // Now you can do validations on the extracted objects:
        assertThat(products, hasSize(2));
        assertThat(products.get(0).get("id"), Matchers.<Object>equalTo(2));
        assertThat(products.get(0).get("name"), Matchers.<Object>equalTo("An ice sculpture"));
        assertThat(products.get(0).get("price"), Matchers.<Object>equalTo(12.5));
        assertThat(products.get(1).get("id"), Matchers.<Object>equalTo(3));
        assertThat(products.get(1).get("name"), Matchers.<Object>equalTo("A blue mouse"));
        assertThat(products.get(1).get("price"), Matchers.<Object>equalTo(25.5));
        get("/lotto").then().body("lotto.lottoId", equalTo(5));


    }
}
