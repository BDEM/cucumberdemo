package com.linos.core;


import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;
import org.junit.runner.RunWith;

///Users/swapanbhowmik/Desktop/POC/src/test/java/com/linos/features/TaskAssign.feature
// tags = "@SmokeTest"  & "@Regression",
//monochrome=true, dryRun=true, strict = true,
@RunWith(Cucumber.class)
@CucumberOptions(features = "src/test/java/com/linos/features", plugin = {"pretty", "html:target/cucumber-reports"},
        glue ={ "com/linos/steps" },  monochrome=true)

public class TestRunner {



}
